import React, { useState } from "react";
import Captions from './Captions';
import { Container, Button, Input } from 'reactstrap';
import { v4 as uuid } from "uuid";
import './App.css';


const Items = () => {
    
    const [items, setItems] = useState([]);
    const [url, setUrl] = useState();

    const guardar = () => {
        localStorage.setItem('mis_items', JSON.stringify(items));
    }

    const recuperar = () => {
        const itemsJson = localStorage.getItem('mis_items');
        const cosas = JSON.parse(itemsJson);
        if (cosas && cosas.length) {
            setItems(cosas);
        } else {
            setItems([]);
        }
    }

    const afegir = () => {
        if (url) {
            const nouItem = {
                imagen: url,
                id: uuid(),
                likes: 0,
            };
            setItems([...items, nouItem]);
            setUrl('');
        }
    }

    const like = (x) => {
        const newItems = items.map((el) => {
            if (el.id === x) {
                el.likes = el.likes + 1
            } 
            return el;
        });
        setItems(newItems)
    };


return (
    <>
        <h1 style={{fontFamily: 'Staatliches', color: '#6c757d'}}>INSTAMOLE</h1>
        <Input style={{fontFamily: 'Staatliches', width: '500px', left: '32%', position: 'relative'}} type="text" value={url} onChange={(ev) => setUrl(ev.target.value)} />
        <Button style={{fontFamily: 'Staatliches', width: '100px', margin: '2px'}} onClick={afegir}>Añadir</Button>
        <Button style={{fontFamily: 'Staatliches', width: '100px', margin: '2px'}} onClick={guardar}>Guardar</Button>
        <Button style={{fontFamily: 'Staatliches', width: '100px', margin: '2px'}} onClick={recuperar}>Recuperar</Button>
        <br />
        <br />

        <Container>
            <Captions items={items} like={like} />
        </Container>
    </>
)
}

export default Items;