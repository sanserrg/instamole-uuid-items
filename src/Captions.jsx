import React from 'react';
import './App.css';
import { Card, CardImg, CardBody, CardTitle, CardText, Button } from 'reactstrap';

const Captions = (props) => {

    const mostra = props.items.map((el) => (
        <>
            <div className='col-4'>
                <Card style={{backgroundColor: '#c9c9e5'}} >
                    <CardImg top width="100%" src={el.imagen} alt="Image" />
                    <CardBody>
                        <CardTitle style={{fontFamily: 'Staatliches'}}>Guacamole</CardTitle>
                        <CardText style={{fontFamily: 'Staatliches'}}>Lorem, ipsum dolor sit amet consectetur adipisicing elit.</CardText>
                        <Button style={{fontFamily: 'Staatliches'}} onClick={() => props.like(el.id)}><i class="fa fa-heart" aria-hidden="true">{el.likes}</i></Button>
                    </CardBody>
                </Card>
            </div>
        </>
    ));

    return (
        <div className="container">
            <div className="row">
                {mostra}
            </div>
        </div>
    );
}

export default Captions;